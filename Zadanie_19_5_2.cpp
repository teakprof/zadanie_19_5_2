﻿#include <iostream>
#include <stdlib.h>
using namespace std;

class Animal //КЛАСС-РОДИТЕЛЬ
{
public:
	virtual void Speak() { cout << "Animal Speak"; } //Виртуальный метод. Звук неизвестного животного
};


class Dog : public Animal //СОЗДАЕМ ПОДКЛАСС животного
{
public:
	void Speak() { cout << "Woof" << "\n"; } //Виртуальный метод. Собака лает
};

class Cat : public Animal //СОЗДАЕМ ПОДКЛАСС животного
{
public:
	void Speak() { cout << "Mau" << "\n"; } //Виртуальный метод. Кот мяукает
};

class Mouse : public Animal //СОЗДАЕМ ПОДКЛАСС животного
{
public:
	void Speak() { cout << "Pi-Pi" << "\n"; } //Виртуальный метод.Мышь пищит
};


void main()
{
	system("CLS");
	Animal* Array[3]; //Объявляем массив указателей на класс Животные
	Animal* ptr;      //Объявили указатель на класс Животные

	int MyWibor;      //Переменная для выбора пользователем
	
	setlocale(LC_ALL, "Russian");
	cout << "Выбери 3 вида животных: \n";
	cout << "1 - СОБАКА \n";
	cout << "2 - КОШКА \n";
	cout << "3 - МЫШЬ \n \n";
	

		for (int i = 0; i < 3; i++) //С помощью цикла заполняем массив указателями на объекты
			                        //Пусть видов животных будет 3
	{
		cout << i + 1 << ". "; //Номер текущего животного
		cin >> MyWibor;  //Пользователь вводит число

		switch (MyWibor) //Основываясь на введенном числе выделяем память для нужного вида животного
		{
		case 1:
			ptr = new Dog; //Если один - выделяем память для создания класса Собака
			break;
		case 2:
			ptr = new Cat; //Если два - выделяем память для создания класса Кошка
			break;
		case 3:
			ptr = new Mouse; //Если три - выделяем память для создания класса Мышь
			break;
		default:
			ptr = new Animal; //Если что-то другое - выделяем память для создания класса Животные
			break;
		}//switch    Выбор сделан

		Array[i] = ptr; //Выбор сделан, память выделена, заносим указатель на созданный объект в массив
	}   //Цикл for завершен

	for (int g = 0; g < 3; g++) Array[g]->Speak(); //Проходим по всему циклу и вызываем метод Speak для каждого элемента

	for (int i = 0; i < 3; i++) delete Array[i]; //Мы выделяли память, значит нужно её освободить.

	system("pause");
}